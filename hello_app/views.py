'''
This is the main file that controls Flask and the routes of your webpage.  
Documentation on its use can be found over here https://flask.palletsprojects.com/en/1.1.x/ 
'''

from datetime import datetime
from flask import Flask, render_template
from . import app

import random
import models.conway

@app.route("/")
def home():
    return render_template("home.jinja2")

@app.route("/about/")
def about():
    return render_template("about.jinja2")

@app.route("/conway/")
@app.route("/conway/<seed>")
@app.route("/conway/<seed>/<steps>")
def conwayView(seed=None, steps=0):
    steps = int(steps)
    if seed is None:
        seed = random.randint(0,9999)
    random.seed(int(seed))
    conwayGame = models.conway.Conway(80, 45)
    conwayGame.populate()
    if steps > 0:
        conwayGame.run(steps)
    conwayStr = str(conwayGame)
    return render_template("conway.jinja2", conwayStr=conwayStr, seed=seed, steps=steps)
    # If there's lots more variables, pass locals() into render_template

@app.route("/hello/")
@app.route("/hello/<name>")
def hello_there(name = None):
    return render_template(
        "hello_there.jinja2",
        name=name,
        date=datetime.now()
    )

@app.route("/api/data")
def get_data():
    return app.send_static_file("data.json")
